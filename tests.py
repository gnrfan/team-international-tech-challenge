import random
import unittest
from data_capture import DataCapture


class TestDataCapture(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.datacapture = DataCapture()
        values = [3, 9, 3, 4, 6]
        for v in values:
            cls.datacapture.add(v)
        cls.stats = cls.datacapture.build_stats()

    def setUp(self):
        self.datacapture = self.__class__.datacapture
        self.stats = self.__class__.stats

    def test_reset(self):
        data_capture = DataCapture()
        data_capture.add(1)
        data_capture.reset()
        self.assertEqual(len(data_capture.values), 0)

    def test_node_structure(self):
        data_capture = DataCapture()
        node = data_capture.get_empty_node()
        self.assertIn("less", node)
        self.assertIn("exact", node)
        self.assertIn("greater", node)
        self.assertIn("prev", node)
        self.assertIn("next", node)
        self.assertEqual(len(node.keys()), 5)

    def test_adding_valid_value(self):
        data_capture = DataCapture()
        ok = data_capture.add(1, raise_exception=False)
        self.assertTrue(ok)

    def test_adding_invalid_value(self):
        data_capture = DataCapture()
        ok = data_capture.add(-1, raise_exception=False)
        self.assertFalse(ok)

    def test_adding_invalid_value_raise_exception(self):
        data_capture = DataCapture()
        self.assertRaisesRegex(
            ValueError,
            r'^The value must be a positive integer.$',
            data_capture.add,
            -1,
            raise_exception=True
        )
        self.assertRaisesRegex(
            ValueError,
            r'^The value must be a positive integer.$',
            data_capture.add,
            "Hello",
            raise_exception=True
        )

    def test_validation_in_less(self):
        self.assertRaisesRegex(
            ValueError,
            r'^The value must be a positive integer.$',
            self.stats.less,
            -1
        )

    def test_validation_in_between(self):
        # Testing if first parameter gets checked
        self.assertRaisesRegex(
            ValueError,
            r'^The value must be a positive integer.$',
            self.stats.between,
            4,
            -1
        )
        # Testing is second parameter gets checked
        self.assertRaisesRegex(
            ValueError,
            r'^The value must be a positive integer.$',
            self.stats.between,
            -1,
            6
        )

    def test_validation_in_greater(self):
        self.assertRaisesRegex(
            ValueError,
            r'^The value must be a positive integer.$',
            self.stats.greater,
            -1
        )

    def test_non_existent_input(self):
        self.assertRaisesRegex(
            ValueError,
            r'^The value [0-9]+ was not part of the inputs.$',
            self.stats.less,
            100
        )

    def test_less(self):
        self.assertEqual(self.stats.less(4), 2)

    def test_between(self):
        self.assertEqual(self.stats.between(3, 6), 4)

    def test_greater(self):
        self.assertEqual(self.stats.greater(4), 2)

    def test_larger_dataset(self):
        data_capture = DataCapture()
        for x in range(1, 6+1):
            for n in range(10000):
                data_capture.add(x)
        stats = data_capture.build_stats()
        self.assertEqual(stats.less(4), 30000)
        self.assertEqual(stats.between(3, 6), 40000)
        self.assertEqual(stats.greater(4), 20000)

    def test_complex_dataset(self):
        values = []
        num_values = random.randint(100, 300)

        for n in range(num_values):
            values.append(random.randint(1, 100))

        distinct_values = list(set(sorted(values)))
        sample = list(sorted(random.sample(distinct_values, 3)))
        lower = sample[0]
        middle = sample[1]
        higher = sample[2]

        middle_count = len([x for x in values if x == middle])
        less_count = len([x for x in values if x < higher])
        between_count = len([x for x in values if x >= lower and x <= higher])
        greater_count = len([x for x in values if x > lower])

        data_capture = DataCapture()
        for v in values:
            data_capture.add(v)

        stats = data_capture.build_stats()

        self.assertEqual(stats.between(middle, middle), middle_count)
        self.assertEqual(stats.less(higher), less_count)
        self.assertEqual(stats.between(lower, higher), between_count)
        self.assertEqual(stats.greater(lower), greater_count)


if __name__ == '__main__':

    unittest.main()
