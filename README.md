# TECHNICAL CHALLENGE SENIOR PYTHON ENGINEER

## Requirements

The DataCapture object accepts numbers and returns an object for querying statistics about the inputs. Specifically, the returned object supports querying how many numbers in the collection are less than a value, greater than a value, or within a range.
Here’s the program skeleton in Python to explain the structure: 

```python
capture = DataCapture()
capture.add(3)
capture.add(9)
capture.add(3)
capture.add(4)
capture.add(6)
stats = capture.build_stats()
stats.less(4) # should return 2 (only two values 3, 3 are less than 4) stats.between(3, 6) # should return 4 (3, 3, 4 and 6 are between 3 and 6)
stats.greater(4) # should return 2 (6 and 9 are the only two values greater than 4)
```

## Challenge conditions

* You cannot import a library that solves it instantly
* The methods `add()`, `less()`, `greater()`, and `between()` should have constant time `O(1)`
* The method `build_stats()` can be at most linear `O(n)` 
* Apply the best practices you know
* Share a public repo with your project

## How to run the code

Just make sure you have a Python3 interpreter available in your system and then run:

```
python challenge.py
```

The output should be:

```
Less than 4:  2
Between 3 and 6:  4
Greater than 4:  2
```

If you want to visualize the values of the internal `stats` data structure, run this command instead:

```
python data_capture.py
```

In order to run the tests do it like this:

```
python tests.py
```

The output should look like this:

```
..............
----------------------------------------------------------------------
Ran 14 tests in 0.023s

OK
```

## Tests

* Input validation
* Internal data structures
* Simple dataset
* Complex dataset
* Large dataset

(c) 2022 Antonio Ognio <antonio@ognio.com>