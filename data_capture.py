from pprint import pprint
from collections import Counter


class StatsMixin(object):

    def check_input_type(self, value, **kwargs):
        raise_exception = kwargs.get('raise_exception', True)
        if type(value) is not int:
            if raise_exception:
                raise ValueError(
                    "The value must be a positive integer."
                )
            else:
                return False
        if value <= 0:
            if raise_exception:
                raise ValueError(
                    "The value must be a positive integer."
                )
            else:
                return False
        return True


class Stats(StatsMixin):

    def __init__(self, stats={}):
        self.stats = stats

    def check_value(self, value, **kwargs):
        """
        Checks if value was part of the inputs
        """
        raise_exception = kwargs.get('raise_exception', True)
        self.check_input_type(value, **kwargs)
        exists = value in self.stats
        if not exists and raise_exception:
            raise ValueError(
                f"The value {value} was not "
                "part of the inputs."
            )
        return exists

    def less(self, value):
        """
        Returns stats on inputs less than the given
        value in O(1)
        """
        self.check_value(value)
        return self.stats[value]['less']

    def greater(self, value):
        """
        Returns stats on inputs greater than the given
        value in O(1)
        """
        self.check_value(value)
        return self.stats[value]['greater']

    def between(self, lower_limit, higher_limit):
        """
        Returns stats on inputs between a lower limit
        and a higher limit in O(1)
        """
        self.check_value(lower_limit)
        self.check_value(higher_limit)
        result = self.stats[higher_limit]['less']
        result += self.stats[higher_limit]['exact']
        result -= self.stats[lower_limit]['less']
        return result


class DataCapture(StatsMixin):

    def __init__(self):
        self.reset()

    def reset(self):
        self.values = []

    def add(self, value, **kwargs):
        """
        Adds an input value
        """
        ok = self.check_input_type(value, **kwargs)
        if ok:
            self.values.append(value)
        return ok

    def get_empty_node(self):
        """
        Creates an empty stats node
        """
        return {
            "less": 0,
            "exact": 0,
            "greater": 0,
            "prev": None,
            "next": None
        }

    def build_stats(self):
        """
        Builds stats data structure in O(N)
        """
        ascending_values = list(sorted(set(self.values)))
        descending_values = list(reversed(ascending_values))
        counter = Counter(self.values)
        prev = None
        acum = 0

        stats = {}
        for value in ascending_values:
            stats[value] = self.get_empty_node()
            stats[value]['less'] = acum
            stats[value]['exact'] = counter[value]
            if prev:
                stats[value]['prev'] = prev
                stats[prev]['next'] = value
            prev = value
            acum += counter[value]

        for value in descending_values:
            node = stats[value]
            if node['prev']:
                prev_node = stats[node['prev']]
                prev_node['greater'] = node['exact'] + node['greater']

        return Stats(stats=stats)


if __name__ == '__main__':

    capture = DataCapture()
    capture.add(3)
    capture.add(9)
    capture.add(3)
    capture.add(4)
    capture.add(6)
    stats = capture.build_stats()
    pprint(stats.stats)
